CC=gcc
obj-m += hjp_stick_driver.o

all:
	make -C /lib/modules/$(shell uname -r)/build/ M=$(shell pwd) modules
	$(CC) hjp_stick_driver.c -o hjp_stick_driver
clean:
	make -C /lib/modules/$(shell uname -r)/build/ M=$(shell pwd) clean
	rm test
