#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/usb.h>

static int hjpusb_probe(struct usb_interface *interface, const struct usb_device_id *id)
{	
		printk("hjpusb: probe module\n");
		printk(KERN_INFO "[*] HanJiPyeong-USB drive (%04X:%04X) plugged\n", id->idVendor, id->idProduct);
		return 0; 
}

static void hjpusb_disconnect(struct usb_interface *interface)
{
		printk("hjpusb: disconnect module\n");
}

static struct usb_device_id hjp_table[] = {
		//0781:5581
		{ USB_DEVICE(0x0781, 0x5406) }, // information is obtained using lsusb at the cmd
		{} 
};
MODULE_DEVICE_TABLE (usb, hjp_table);

static struct usb_driver hjpusb_driver = {
	   name: "HanJiPyeong-USB stick-driver",
	   id_table: hjp_table, //usb_device_id
	   probe: hjpusb_probe,
	   disconnect: hjpusb_disconnect,
};
 

static int __init hjpusb_init(void)
{
	   int result = -1; 
	   printk(KERN_INFO "[*] HanJiPyeong-USB Constructor of driver");
	   printk(KERN_INFO "\tRegistering hjpusb driver with Kernel");
	   result = usb_register(&hjpusb_driver);
	   printk(KERN_INFO "\thjpusb: driver registered successfully");
	   return result;
}


static void __exit hjpusb_exit(void)
{	
	   printk(KERN_INFO "[*] HanJiPyeong-USB Destructor of driver");
	   usb_deregister(&hjpusb_driver);
	   printk("hjpusb: module deregistered");
}

module_init(hjpusb_init);
module_exit(hjpusb_exit);

MODULE_AUTHOR("Tim Han Ji Pyeong: Siti Aulia, Faza Siti, Naufal P");
MODULE_LICENSE("GPL");