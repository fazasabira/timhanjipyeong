1. Membuat hjp_stick_driver.c 
2. Membuat Makefile 
3. Compile Makefile yang sudah dibuat dengan command make  
4. Setelah memastikan sudah terdapat hjp_stick_driver.ko, dilakukan install module  <br /> 
insmod hjp_stick_driver.ko  <br /> 
Insmod usb-storage  <br /> 
5. Plugged USB, kemudian buka VirtualBox > Pilih tab Devices > USB > Pilih nama USB yang disambungkan. Cek apakah USB sudah tersambung pada VirtualBox atau belum dengan cara lsusb  
6. Cek di dmesg  
7. Mengubah parameter delay_use sesuai yang diinginkan dengan cara  <br /> 
 echo 5 > /sys/module/usb_storage/parameters/delay_use  <br /> 
*5 adalah angka untuk delay_use  <br /> 
8. Cek kembali di dmesg  
